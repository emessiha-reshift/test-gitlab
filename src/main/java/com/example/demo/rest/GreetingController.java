package com.example.demo.rest;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

	@GetMapping("/greeting")
	public String greeting(@RequestParam(value = "name", defaultValue = "World") String name, HttpServletResponse response) throws Exception{
		if (name.contains("http")) {
			response.sendRedirect(name);
		}
		return "Greetings from demo, " + name + "! ";
	}
}