#!/bin/bash
set -e
MY_NAME=$( basename $0 )
MY_DIR=$( cd $(dirname $0) && pwd )
MY_REAL_PATH=$(realpath "$MY_DIR")
MY_DIR_NAME=$(dirname "$MY_DIR")

cd $MY_REAL_PATH
mvn clean install 
java -jar target/demo-0.0.1-SNAPSHOT.jar
exit 0
